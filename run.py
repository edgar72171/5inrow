#!/usr/bin/env python3

import game
import pygame
import logging

""" Execution: general configuration. """

if __name__ == '__main__':
    logging.basicConfig(filename='var/5inrow.log', encoding='utf-8', level=logging.DEBUG)
    pygame.init()
    pygame.mouse.set_visible(True)

    game = game.Game()
    game.start()
