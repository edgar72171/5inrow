import logging
import pickle
import pygame
import numpy as np

from typing import Union

from player import Player
from lib.pygame.text import Text


class Desk:
    """
        Draw's grid and players crosses/circles on a game desk: Transform game_play: ((1,O,0), (0, 2, 0), (0, 0, 1)) > into crosses and circles drawings.
        Holds game_play (who played where).
    """
    def __init__(self, surface, tiles: int, tile_outer_size: int, tile_inner_size: int, player1: Player, player2: Player):
        self.logger = logging.getLogger(__file__)
        self.surface = surface
        self.tiles: int = tiles
        self.position: pygame.Rect = pygame.Rect(0, 0, tiles*tile_outer_size+1, tiles*tile_outer_size+1)
        self.tile_outer_size: int = tile_outer_size
        self.tile_inner_size: int = tile_inner_size
        self.grid_color: tuple[int, int, int] = (128, 128, 128)
        self.player1: Player = player1
        self.player2: Player = player2
        self.ai_score_font = pygame.font.Font(pygame.font.get_default_font(), tile_inner_size//3)
        self.ai_scores: Union[None, np.array] = None
        self.game_play: np.array = np.array(tuple(tiles * [None] for _ in range(tiles)))
        self.turn: int = 1
        self.playing: Player = player1
        self.fifth_in_line: Union[None, tuple[tuple[int, int], tuple[int, int]]] = None
        self.last_play = None

    def get_pixel_coordinates(self, grid_coordinates: tuple[int, int]) -> tuple[int, int]:
        x, y = grid_coordinates
        return (x * self.tile_outer_size + self.tile_outer_size // 2,
                y * self.tile_outer_size + self.tile_outer_size // 2)

    def get_grid_coordinates(self, pixel_coordinates: tuple[int, int]) -> tuple[int, int]:
        return (pixel_coordinates[0] // self.tile_outer_size,
                pixel_coordinates[1] // self.tile_outer_size)

    def clear(self):
        self.surface.fill((0, 0, 0))

    def draw_grid(self):
        for column in range(self.tiles+1):
            pygame.draw.line(self.surface, self.grid_color, start_pos=(column*self.tile_outer_size, 0), end_pos=(column*self.tile_outer_size, self.tiles*self.tile_outer_size), width=1)
        for row in range(self.tiles+1):
            pygame.draw.line(self.surface, self.grid_color, start_pos=(0, row*self.tile_outer_size), end_pos=(self.tiles*self.tile_outer_size, row*self.tile_outer_size), width=1)

    def draw_last_play(self):
        if self.last_play:
            x, y = self.get_pixel_coordinates(self.last_play)
            pygame.draw.rect(self.surface, (200, 200, 200), rect=(x-self.tile_outer_size//2, y-self.tile_outer_size//2, self.tile_outer_size+1, self.tile_outer_size+1), width=1)

    def draw_game_play(self):
        for y in range(self.tiles):
            for x in range(self.tiles):
                player = self.game_play[y, x]
                if player:
                    player.draw(self.surface, self.get_pixel_coordinates((x, y)), self.tile_inner_size)

    def draw_fifth_in_line(self):
        if self.fifth_in_line:
            begin, end = self.fifth_in_line
            self.logger.debug(f'fifth in line: {begin}-{end}')
            player = self.game_play[begin[1]][begin[0]]
            pygame.draw.line(self.surface, player.color, start_pos=self.get_pixel_coordinates(begin), end_pos=self.get_pixel_coordinates(end), width=10)

    def draw_ai_scores_if_needed(self):
        if self.ai_scores is not None:
            for y in range(self.tiles):
                for x in range(self.tiles):
                    if isinstance(self.ai_scores[y, x], tuple):
                        pos_x, pos_y = self.get_pixel_coordinates((x, y))
                        pos_x, pos_y = pos_x-self.tile_outer_size//2+1, pos_y-self.tile_outer_size//2+1
                        new_line_px = 0
                        for score in self.ai_scores[y, x]:
                            Text(self.surface, pos_x, pos_y+new_line_px, str(score), self.ai_score_font, pygame.Color(*3*[max((min(20 + 10 * score, 255)), 0)]), pygame.Color(0, 0, 0)).draw()
                            new_line_px += self.tile_outer_size//5

    def draw(self):
        self.clear()
        self.draw_grid()
        self.draw_last_play()
        self.draw_game_play()
        self.draw_fifth_in_line()
        self.draw_ai_scores_if_needed()

    def play(self, grid_coordinates: tuple[int, int]) -> bool:
        x, y = grid_coordinates
        if not 0 <= x < self.tiles or not 0 <= y < self.tiles or self.game_play[y, x]:
            return False
        self.game_play[y, x] = self.playing
        self.turn += 1 if self.playing == self.player2 else 0
        self.last_play = x, y
        self.fifth_in_line = self.find_fifth_in_line(x, y)
        self.playing = None if self.fifth_in_line else self.player1 if self.playing == self.player2 else self.player2
        return bool(self.playing)

    def find_fifth_in_line(self, x, y) -> tuple[tuple[int, int], tuple[int, int]]:
        up, left, up_left, up_right = (0, -1), (-1, 0), (-1, -1), (+1, -1)
        for direction in (up, left, up_left, up_right):
            stones_one_direction = self.stones_in_direction(x, y, +direction[0], +direction[1], stone=self.game_play[y, x])
            stones_other_direction = self.stones_in_direction(x, y, -direction[0], -direction[1], stone=self.game_play[y, x])
            if stones_one_direction + 1 + stones_other_direction >= 5:
                return ((x + stones_one_direction * direction[0], y + stones_one_direction * direction[1]),
                        (x + stones_other_direction * -direction[0], y + stones_other_direction * -direction[1]))

    def stones_in_direction(self, x: int, y: int, dx: int, dy: int, stone: Player) -> int:
        stones = 0
        x += dx
        y += dy

        while 0 <= x < self.tiles and 0 <= y < self.tiles:
            next_stone = self.game_play[y, x]
            if next_stone == stone:
                stones += 1
            else:
                break
            x += dx
            y += dy

        return stones

    def show_ai_scores(self, ai_scores: Union[None, np.array]):
        self.ai_scores = ai_scores

    def new_game(self):
        self.game_play[:] = None
        if self.ai_scores is not None:
            self.ai_scores[:] = None
        self.turn = 1
        self.playing = self.player1
        self.fifth_in_line = None
        self.last_play = None

    def save_game(self, filename):
        with open(filename, 'wb') as file:
            pickle.dump((self.game_play, self.turn, self.playing, self.fifth_in_line, self.last_play), file)

    def load_game(self, filename):
        if self.ai_scores is not None:
            self.ai_scores[:] = None

        with open(filename, 'rb') as file:
            try:
                self.game_play, self.turn, self.playing, self.fifth_in_line, self.last_play = pickle.load(file)
            except FileNotFoundError as ex:
                self.logger.exception(ex)
