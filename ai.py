import copy
import numpy as np

from player import Player
from lib import utils


class AI:
    BASE_VALUE = 5
    BLOCKED = 40
    NOT_BLOCKED = 0
    FUTURE_WEIGHT = 4
    WIN_SCORE = BASE_VALUE**5 - 2*BLOCKED

    def __init__(self, player1: Player, player2: Player) -> None:
        self.player1 = player1
        self.player2 = player2
        self.ai_scores = None
        self.y_max = None
        self.x_max = None
        self.level = 0

    def set_level(self, level: int):
        self.level = level

    def play(self, game_play: np.array, player: Player) -> tuple[int, int]:
        """ Simulate each available move to pick one leading to the best score. """
        self.y_max = len(game_play)     # rows
        self.x_max = len(game_play[0])  # columns
        if self.ai_scores is None:
            self.ai_scores = np.full_like(game_play, np.nan, dtype=None)
        else:
            self.ai_scores[:] = None

        first_move = (game_play == None).all()
        if first_move:
            return self.x_max//2, self.y_max//2  # center

        if self.level == 0:
            position, _, _ = self.find_best_position(game_play, player)
        else:
            position = self.simulate_moves(game_play, player)

        return position

    def simulate_moves(self, game_play: np.array, player: Player) -> tuple[int, int]:
        opponent = self.player2 if player == self.player1 else self.player1
        best_offense_position = best_defense_position = None
        best_offense_score = -self.WIN_SCORE*self.WIN_SCORE**(self.level+2)
        best_defense_score = self.WIN_SCORE*self.WIN_SCORE**(self.level+2)
        situations = ((copy.deepcopy(game_play), x, y, player, opponent) for y in range(self.y_max) for x in range(self.x_max) if self.is_worth_of_exploring(game_play, x, y))
        for (offense_player, defense_player, next_player, next_opponent), _, x, y, _, _ in utils.start_in_processes(self.simulate_moves_in_parallel, situations):
            if best_offense_score < offense_player + next_player and offense_player + next_player >= defense_player + next_opponent:
                best_offense_score = offense_player + next_player
                best_offense_position = x, y

            if best_defense_score > defense_player + next_opponent:
                best_defense_score = defense_player + next_opponent
                best_defense_position = x, y
            self.ai_scores[y, x] = (offense_player, defense_player, next_player, next_opponent)

        print(f'{best_offense_score}:{best_offense_position} vs {best_defense_score}:{best_defense_position}')  # FIXME: logger
        return best_offense_position if best_offense_position and best_offense_score >= best_defense_score else best_defense_position

    def simulate_moves_in_parallel(self, game_play: np.array, x: int, y: int, player: Player, opponent: Player) -> tuple[int, int, int, int]:
        offense_player = self.evaluate_position(game_play, x, y, player)
        game_play[y, x] = player

        offense_opponent, _ = self.play_best_position(game_play, opponent)

        if offense_player >= self.WIN_SCORE:
            return self.WIN_SCORE**(self.level+1), offense_opponent, 0, 0
        elif offense_opponent >= self.WIN_SCORE:
            return offense_player, self.WIN_SCORE**(self.level+1), 0, 0

        next_player, next_opponent = self.simulate_moves_recursive(game_play, player, opponent, self.level - 1)
        return offense_player, offense_opponent, next_player//self.FUTURE_WEIGHT, next_opponent//self.FUTURE_WEIGHT

    def simulate_moves_recursive(self, game_play: np.array, player: Player, opponent: Player, level, debug: bool = False) -> tuple[int, int]:
        if level == 0:
            player_move, offense_player, _ = self.find_best_position(game_play, player)
            opponent_move, offense_opponent, _ = self.find_best_position(game_play, opponent)
            if debug:
                print(f'simulate_moves_recursive: {player} {offense_player}:{player_move} / {offense_opponent}:{opponent_move}')
            return offense_player, offense_opponent

        offense_player, _ = self.play_best_position(game_play, player)
        offense_opponent, _ = self.play_best_position(game_play, opponent)

        if offense_player >= self.WIN_SCORE:
            return self.WIN_SCORE**(level+1), offense_opponent
        elif offense_opponent >= self.WIN_SCORE:
            return offense_player, self.WIN_SCORE**(level+1)

        next_player, next_opponent = self.simulate_moves_recursive(game_play, player, opponent, level - 1)
        return offense_player + next_player//self.FUTURE_WEIGHT, offense_opponent + next_opponent//self.FUTURE_WEIGHT

    def play_best_position(self, game_play: np.array, player: Player, debug: bool = False) -> tuple[int, int]:
        (x, y), offense, defense = self.find_best_position(game_play, player, debug)
        game_play[y, x] = player
        if debug:
            print(f'play_best_position: {player} ({x},{y})')
        return offense, defense

    def find_best_position(self, game_play: np.array, player: Player, debug: bool = False) -> tuple[tuple[int, int], int, int]:
        """ Calculate coordinates and score of the best move. """
        opponent = self.player2 if player == self.player1 else self.player1
        best_offense_position = best_defense_position = (0, 0)
        best_offense_score = best_defense_score = -1
        for y in range(self.y_max):
            for x in range(self.x_max):
                if self.is_worth_of_exploring(game_play, x, y):
                    offense_score = self.evaluate_position(game_play, x, y, player)
                    defense_score = self.evaluate_position(game_play, x, y, opponent)
                    if self.level == 0:
                        self.ai_scores[y, x] = (offense_score, defense_score)
                    if offense_score > best_offense_score:
                        best_offense_score = offense_score
                        best_offense_position = (x, y)
                    if defense_score > best_defense_score:
                        best_defense_score = defense_score
                        best_defense_position = (x, y)

        if (best_offense_score >= self.BASE_VALUE**3 and best_defense_score < self.BASE_VALUE**4-self.BLOCKED) or \
                best_offense_score >= self.BASE_VALUE**4-self.BLOCKED or best_offense_score >= best_defense_score:
            return best_offense_position, best_offense_score, best_defense_score
        else:
            return best_defense_position, best_offense_score, best_defense_score

    def evaluate_position(self, game_play: np.array, x, y, player) -> int:
        """ Sum fitness value of given position from all directions (multiply each direction with opposite). """
        score = 0
        up, left, up_left, up_right = (0, -1), (-1, 0), (-1, -1), (+1, -1)
        for direction in (up, left, up_left, up_right):
            direction_score1, blocked1 = self.evaluate_direction(game_play, x, y, +direction[0], +direction[1], player)
            direction_score2, blocked2 = self.evaluate_direction(game_play, x, y, -direction[0], -direction[1], player)
            # TODO: if blocked on both ends + no space for fifth > return 0
            score += max(direction_score1*direction_score2 - (blocked1 + blocked2), 0)
        return score

    def evaluate_direction(self, game_play: np.array, x: int, y: int, dx: int, dy: int, player: Player) -> tuple[int, int]:
        """ Calculate score from given position in given direction for given player. """
        score = 1
        x += dx
        y += dy
        while 0 <= x < self.x_max and 0 <= y < self.y_max:
            occupation = game_play[y, x]
            if not occupation:
                return score, self.NOT_BLOCKED
            elif occupation == player:
                score *= self.BASE_VALUE
            else:
                break  # direction closed by opponent
            x += dx
            y += dy

        return score, self.BLOCKED  # direction is closed either by opponent or end of the desk

    def is_worth_of_exploring(self, game_play: np.array, x: int, y: int) -> bool:
        if game_play[y, x]:  # skip occupied fields
            return False
        return game_play[max(0, y-2):min(self.y_max-1, y+3),
                         max(0, x-2):min(self.x_max-1, x+3)].any()  # skip fields with empty two fields in every direction
