from pygame import Surface, Rect, Color
from pygame.font import Font, get_default_font

from lib.pygame.drawable import DrawAble
from lib.pygame.text import TextCentered


class Menu:
    """ Menu definition: positions, drawing. """
    def __init__(self, surface: Surface, position: Rect) -> None:
        item_size = 48
        self.surface = surface
        self.position = position
        self.font = Font(get_default_font(), 30)
        self.text_color: Color = Color(64, 64, 200)
        self.background_color: Color = Color(64, 64, 64)

        for line, (variable, text) in enumerate({'new_game': 'New Game',
                                                 'save_game': 'Save Game',
                                                 'load_game': 'Load Game',
                                                 'roll_back': 'Roll Back',  # TODO: Roll forward
                                                 'turn': 'Turn: 1',
                                                 'playing': 'Playing: None',
                                                 'ai_level': 'AI level: 0',
                                                 'show_ai': 'Show AI DEBUG'}.items(), start=5):
            self.__setattr__(variable, TextCentered(surface, (position.x, position.y+line*item_size, position.width, item_size), text, self.font, self.text_color, self.background_color))

        self.elements_to_draw = [element for element in self.__dict__.values() if isinstance(element, DrawAble)]

    def draw(self) -> None:
        for element in self.elements_to_draw:
            element.draw()
