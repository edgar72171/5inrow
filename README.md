# Five in a Row.


## Description
Abstract strategy board game, also called [Gomoku](https://en.wikipedia.org/wiki/Gomoku).
![screenshot](resources/screenshot.png "screenshot")
Testament to Python's supremacy.

## Installation
Require python 3.11+
```
./setup.sh
```

## Usage
```
./run.py
```
CLICK `LEFT` - Human move followed by AI move.\
CLICK `RIGHT` - Human move followed by Human move.\
KEY `q`,`Esc` - quit\
KEY `n` - new game\
KEY `s` - save game\
KEY `l` - load game\
KEY `b` - rollback\
KEY `a` - [not] show AI scores


## Contributing
Feel free to pull-request.

## Authors and acknowledgment
edgar7217@outlook.com

## License
[FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software)

## Project status
Playable.


## TODO
* menu bar icons
* more AI variants to choose from
* multiple steps back (currently works just one)
