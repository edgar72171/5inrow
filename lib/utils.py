
import os
import concurrent.futures
from typing import Generator


def start_in_threads(fn: callable, args_i: iter, max_workers=2 * os.cpu_count()) -> Generator:
    executor = concurrent.futures.ThreadPoolExecutor(max_workers)
    task_to_argument = {executor.submit(fn, *args): args for args in args_i}
    return ((task_done.result(), *(task_to_argument[task_done])) for task_done in concurrent.futures.as_completed(task_to_argument))


def start_in_processes(fn: callable, args_i: iter, max_workers=2 * os.cpu_count()) -> Generator:
    """Like start_in_threads, but in processes. Good for CPU heavy."""
    executor = concurrent.futures.ProcessPoolExecutor(max_workers)
    task_to_argument = {executor.submit(fn, *args): args for args in args_i}
    return ((task_done.result(), *(task_to_argument[task_done])) for task_done in concurrent.futures.as_completed(task_to_argument))
