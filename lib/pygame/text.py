from pygame import Surface, Rect, Color, draw
from pygame.font import Font

from .drawable import DrawAble


class Text(DrawAble):
    def __init__(self, surface, x: int, y: int, text: str, font: Font, color: Color, background: Color, border: int = 1) -> None:
        self.surface: Surface = surface
        self.text: str = text
        self.font: Font = font
        self.color: Color = color
        self.background: Color = background
        self.border = border
        self.image = self.font.render(self.text, True, self.color)
        self.position: Rect = Rect(x, y, self.image.get_width(), self.image.get_height())
        self.outer_position: Rect = self.position

    def set_text(self, text: str) -> None:
        self.text = text
        self.image = self.font.render(self.text, True, self.color)
        self.position = Rect(self.position.x, self.position.y, self.image.get_width(), self.image.get_height())

    def draw(self) -> None:
        draw.rect(self.surface, self.background, self.outer_position, self.border)
        self.surface.blit(self.image, self.position)


class TextCentered(Text):
    def __init__(self, surface, position: tuple[int, int, int, int], text: str, font: Font, color: Color, background: Color, border: int = 1) -> None:
        x, y, w, h = position
        super().__init__(surface, x, y, text, font, color, background, border)
        self.outer_position = Rect(position)
        self.position.width = w
        self.position.height = h
        self.position.x = self.position.centerx - self.image.get_width() // 2
        self.position.y = self.position.centery - self.image.get_height() // 2
