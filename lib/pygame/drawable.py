from abc import ABC, abstractmethod


class DrawAble(ABC):
    @abstractmethod
    def draw(self) -> None:
        pass
