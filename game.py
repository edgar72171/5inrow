import pygame
from pygame import Rect
from typing import Iterable

from ai import AI
from desk import Desk
from menu import Menu
from player import Player, PlayerCross, PlayerCircle


class Game:
    """ I/O evens handling: wiring all together. """
    RED_COLOR: tuple[int, int, int] = (255, 0, 0)
    GREEN_COLOR: tuple[int, int, int] = (0, 255, 0)
    LEFT_M_BUTTON: int = 1
    MID_M_BUTTON: int = 2
    RIGHT_M_BUTTON: int = 3

    def __init__(self, tiles=19, tile_outer_size=48, tile_inner_size=30) -> None:
        self.player1: Player = PlayerCross(self.RED_COLOR)
        self.player2: Player = PlayerCircle(self.GREEN_COLOR)
        desk_width = tiles*tile_outer_size+1
        menu_width = 300
        self.screen = pygame.display.set_mode((desk_width+menu_width, desk_width))
        self.desk: Desk = Desk(self.screen, tiles, tile_outer_size, tile_inner_size, player1=self.player1, player2=self.player2)
        self.menu: Menu = Menu(self.screen, Rect(desk_width, 0, menu_width, desk_width))
        self.ai: AI = AI(self.player1, self.player2)
        self.frame_rate: int = 10
        self.running: bool = True
        self.game_play_bkp = './var/game'
        self.game_play_file = './var/game.sav'
        pygame.display.set_icon(pygame.image.load('resources/icon.png'))

    def start(self) -> None:
        clock = pygame.time.Clock()

        while self.running:
            clock.tick(self.frame_rate)
            self.handle_events()
            self.desk.draw()
            self.menu.draw()
            pygame.display.update()

    def handle_events(self) -> None:
        def increase_ai():
            if self.ai.level < 99:
                self.ai.set_level(level=self.ai.level + 1)
                self.menu.ai_level.set_text(self.menu.ai_level.text[:-2] + f'{self.ai.level:>2}')

        def decrease_ai():
            if self.ai.level > 0:
                self.ai.set_level(level=self.ai.level - 1)
                self.menu.ai_level.set_text(self.menu.ai_level.text[:-2] + f'{self.ai.level:>2}')

        buttons_wiring = ((self.menu.new_game, self.desk.new_game, {}, 'n'),
                          (self.menu.save_game, self.desk.save_game, {'filename': self.game_play_file}, 's'),
                          (self.menu.load_game, self.desk.load_game, {'filename': self.game_play_file}, 'l'),
                          (self.menu.roll_back, self.desk.load_game, {'filename': self.game_play_bkp + str(self.desk.turn-1 if self.desk.turn > 1 else 1)}, 'b'),
                          (self.menu.ai_level, increase_ai, {}, '+'),
                          (self.menu.ai_level, decrease_ai, {}, '-'),
                          (self.menu.show_ai, self.desk.show_ai_scores, {'ai_scores': self.ai.ai_scores if self.desk.ai_scores is None else None}, 'a'))

        for event in pygame.event.get():
            match event.type:
                case pygame.MOUSEBUTTONDOWN:
                    self.handle_mouse_button(event, buttons_wiring)
                case pygame.KEYDOWN:
                    self.handle_key_down(event, buttons_wiring)
                case pygame.QUIT:
                    self.running = False

        self.menu.turn.set_text(text=f'Turn: {self.desk.turn}')
        self.menu.playing.set_text(text=f'Playing: {self.desk.playing}')

    def handle_mouse_button(self, event: pygame.event, buttons_wiring: Iterable) -> None:
        mouse_position = pygame.mouse.get_pos()
        match event.button:
            case self.LEFT_M_BUTTON:
                if self.desk.playing and self.desk.position.collidepoint(mouse_position):
                    self.desk.save_game(self.game_play_bkp + str(self.desk.turn))
                    if self.desk.play(self.desk.get_grid_coordinates(mouse_position)):
                        self.desk.play(self.ai.play(game_play=self.desk.game_play, player=self.desk.playing))
                elif self.menu.position.collidepoint(mouse_position):
                    for button, action, kw_args, _ in buttons_wiring:
                        if button.position.collidepoint(mouse_position):
                            action(**kw_args)
                            break

            case self.RIGHT_M_BUTTON:  # FIXME: remove once UI capable to take over this mode
                self.desk.save_game(self.game_play_bkp + str(self.desk.turn))
                self.desk.play(self.desk.get_grid_coordinates(pygame.mouse.get_pos()))
                self.menu.playing.set_text(text=f'Playing: {self.desk.playing}')

    def handle_key_down(self, event: pygame.event, buttons_wiring: Iterable):
        key_name = pygame.key.name(event.key)
        if key_name == 'q' or key_name == 'escape':
            self.running = False
        else:
            for _, action, kw_args, key in buttons_wiring:
                if key_name == key:
                    action(**kw_args)
                    break
