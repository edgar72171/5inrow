import pygame
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any


@dataclass
class Player(ABC):
    """ Draw player marker into given pixel coordinates. """
    color: tuple[int, int, int]

    @abstractmethod
    def draw(self, surface: Any, center: tuple[int, int], size: int) -> None:
        pass


class PlayerCross(Player):
    def __str__(self):
        return 'X'

    def draw(self, surface: Any, center: tuple[int, int], size: int) -> None:
        x, y = center
        pygame.draw.line(surface, self.color, start_pos=(x - size/2, y - size/2), end_pos=(x + size/2, y + size/2), width=10)
        pygame.draw.line(surface, self.color, start_pos=(x + size/2, y - size/2), end_pos=(x - size/2, y + size/2), width=10)


class PlayerCircle(Player):
    def __str__(self):
        return 'O'

    def draw(self, surface: Any, center: tuple[int, int], size: int) -> None:
        pygame.draw.circle(surface, self.color, center, size / 2, width=5)
